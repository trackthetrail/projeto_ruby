class Post < ApplicationRecord
  belongs_to :user, optional: true
  validates :title, presence: true,  length:{in: 1..50}
end

