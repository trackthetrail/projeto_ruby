class User < ApplicationRecord
	has_secure_password
	has_many :posts, dependent: :destroy
	validates :password, presence: true, length:{minimum:3}
	validates :name, presence: true
	validates :email, presence: true



	def User.digest(string)
  cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
  BCrypt::Password.create(string, cost: cost)
	end
end
